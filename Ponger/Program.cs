﻿using System;
using PingPong.Shared;

namespace Ponger
{
    public sealed class Program
    {
        public static void Main(string[] args)
        {
            using (var connection = new PingPongConnection(PingPongConnection.Name.Ponger))
            {
                Console.WriteLine($"PONGER with id {connection.id} is running");
                Console.ReadLine();
            }
        }
    }
}
