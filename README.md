# PingPong

## Requirements:

- .NET 5
- Docker
- docker-compose

## Setup:

- Create `.env` file in the project root and copy text from `.env.example`
- `docker-compose up -d`

## Start:

**NOTE:** Ponger starts first.

### Pinger

- `cd Pinger`
- `dotnet run`

### Ponger

- `cd Ponger`
- `dotnet run`

## Notes:

- `SendMessageToQueue` is located at `RabbitMQ.Wrapper/Queue.cs` and called `Send`
- `ListenQueue` is located at `RabbitMQ.Wrapper/Consumer.cs` and called `event RecievedMessage`
- RabbitMQ connection string is `amqp://guest:guest@localhost:5672`
- RabbitMQ admin url is `http://localhost:15672`
- Main logic is located at `PingPong.Shared/PingPongConnection.cs`
