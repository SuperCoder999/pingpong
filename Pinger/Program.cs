﻿using System;
using PingPong.Shared;

namespace Pinger
{
    public sealed class Program
    {
        public static void Main(string[] args)
        {
            using (var connection = new PingPongConnection(PingPongConnection.Name.Pinger, true))
            {
                Console.WriteLine($"PINGER with id {connection.id} is running");
                Console.ReadLine();
            }
        }
    }
}
