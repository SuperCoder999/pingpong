﻿using System;
using System.Text;
using dotenv.net;

namespace PingPong.Shared
{
    public static class Env
    {
        static Env()
        {
            DotEnv.Load(new DotEnvOptions(true, new string[] { "../.env" }, Encoding.UTF8));
        }

        public static string Get(string name)
        {
            return Environment.GetEnvironmentVariable(name);
        }
    }
}
