using System;
using System.Threading;
using RabbitMQ.Wrapper;

namespace PingPong.Shared
{
    public sealed class PingPongConnection : IDisposable
    {
        private readonly Queue queue;
        private readonly Queue inverseQueue;
        private readonly Connection connection;
        private readonly string message;
        private readonly string name;
        public readonly string id = Utils.GenerateRandomString();

        public PingPongConnection(string name, bool autoSend = false)
        {
            string inverseName = Name.GetInverse(name);

            if (inverseName == null)
            {
                throw new InvalidOperationException("Trying to create PingPongConnection with invalid name");
            }

            this.name = name;
            message = Env.Get($"{name}_MESSAGE");

            string exchangeName = Env.Get("EXCHANGE_NAME");

            string queueName = Env.Get($"{name}_QUEUE_NAME");
            string routingKey = Env.Get($"{name}_ROUTING_KEY");

            string inverseQueueName = Env.Get($"{inverseName}_QUEUE_NAME");
            string inverseRoutingKey = Env.Get($"{inverseName}_ROUTING_KEY");

            connection = new Connection(Env.Get("RABBITMQ_CONNECTION_STRING"));

            Exchange exchange = connection
                .CreateChannel()
                .CreateExchange(exchangeName, Exchange.Types.Direct);

            queue = exchange.CreateQueue(queueName, routingKey);
            inverseQueue = exchange.CreateQueue(inverseQueueName, inverseRoutingKey);
            queue.CreateConsumer(true).ReceivedMessage += OnRecieved; // Message processing CAN'T be unsuccessful

            if (autoSend)
            {
                Send();
            }
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        private void OnRecieved(object sender, RecievedEventArgs args)
        {
            Console.WriteLine($"[{DateTime.Now.ToString()}] {args.Message}");
            DelayedSend();
        }

        private void Send()
        {
            inverseQueue.Send($"{message} from {name} with id {id}");
        }

        private void DelayedSend()
        {
            Thread.Sleep(2500);
            Send();
        }

        public static class Name
        {
            public const string Pinger = "PINGER";
            public const string Ponger = "PONGER";

            internal static string GetInverse(string name)
            {
                if (name == Pinger)
                {
                    return Ponger;
                }
                else if (name == Ponger)
                {
                    return Pinger;
                }

                return null;
            }
        }
    }
}
