using System;

namespace PingPong.Shared
{
    public static class Utils
    {
        private const string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private static readonly Random random = new Random();

        public static string GenerateRandomString(int length = 8)
        {
            string result = "";

            for (int i = 0; i < length; i++)
            {
                int letterIndex = random.Next(0, alphabet.Length);
                char letter = alphabet[letterIndex];
                result += letter;
            }

            return result;
        }
    }
}
