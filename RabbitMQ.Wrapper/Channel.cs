using System;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper
{
    public sealed class Channel : IDisposable
    {
        public Connection Connection { get; }
        internal IModel RabbitChannel { get; }

        public Channel(Connection connection)
        {
            Connection = connection;
            RabbitChannel = connection.RabbitConnection.CreateModel();
        }

        public Exchange CreateExchange(string name, string type)
        {
            return new Exchange(name, type, this);
        }

        public void Dispose()
        {
            RabbitChannel.Dispose();
        }
    }
}
