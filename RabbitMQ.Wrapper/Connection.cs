﻿using System;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper
{
    public sealed class Connection : IDisposable
    {
        internal IConnection RabbitConnection { get; }
        internal event Action ChannelDispose;

        public Connection(string uri)
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                Uri = new Uri(uri)
            };

            RabbitConnection = factory.CreateConnection();
        }

        public Channel CreateChannel()
        {
            Channel channel = new Channel(this);
            ChannelDispose += channel.Dispose;
            return channel;
        }

        public void Dispose()
        {
            ChannelDispose?.Invoke();
            RabbitConnection.Dispose();
        }
    }
}
