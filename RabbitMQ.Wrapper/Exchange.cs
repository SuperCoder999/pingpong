using RabbitMQ.Client;

namespace RabbitMQ.Wrapper
{
    public sealed class Exchange
    {
        public string Name { get; }
        public string Type { get; }
        public Channel Channel { get; }

        public Exchange(string name, string type, Channel channel)
        {
            Name = name;
            Type = type;
            Channel = channel;

            Channel.RabbitChannel.ExchangeDeclare(Name, Type);
        }

        public Queue CreateQueue(
            string name,
            string routingKey,
            bool durable = false,
            bool exclusive = false,
            bool autoDelete = false
        )
        {
            return new Queue(name, routingKey, durable, exclusive, autoDelete, this);
        }

        public static class Types
        {
            public const string Fanout = ExchangeType.Fanout;
            public const string Direct = ExchangeType.Direct;
            public const string Topic = ExchangeType.Topic;
            public const string Headers = ExchangeType.Headers;
        }
    }
}
