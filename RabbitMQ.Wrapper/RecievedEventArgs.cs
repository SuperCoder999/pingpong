using System;

namespace RabbitMQ.Wrapper
{
    public struct RecievedEventArgs
    {
        public string Message { get; internal set; }
        public Action Ack { get; internal set; }
        public Action<bool> Nack { get; internal set; }
    }
}
