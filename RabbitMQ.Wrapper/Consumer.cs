using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper
{
    public sealed class Consumer
    {
        public Queue Queue { get; }
        internal EventingBasicConsumer RabbitConsumer { get; }
        public event EventHandler<RecievedEventArgs> ReceivedMessage;

        public Consumer(bool autoAck, Queue queue)
        {
            Queue = queue;
            RabbitConsumer = new EventingBasicConsumer(Queue.Exchange.Channel.RabbitChannel);

            RabbitConsumer.Received += OnRecieved;
            Queue.Exchange.Channel.RabbitChannel.BasicConsume(Queue.Name, autoAck, RabbitConsumer);
        }

        private void OnRecieved(object sender, BasicDeliverEventArgs args)
        {
            string message = Encoding.UTF8.GetString(args.Body.ToArray());

            ReceivedMessage?.Invoke(sender, new RecievedEventArgs
            {
                Message = message,
                Ack = () => Queue.Exchange.Channel.RabbitChannel.BasicAck(args.DeliveryTag, false),
                Nack = (requeue) => Queue.Exchange.Channel.RabbitChannel.BasicNack(args.DeliveryTag, false, requeue),
            });
        }
    }
}
