using System.Text;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper
{
    public sealed class Queue
    {
        public string Name { get; }
        public string RoutingKey { get; }
        public Exchange Exchange { get; }

        public Queue(
            string name,
            string routingKey,
            bool durable,
            bool exclusive,
            bool autoDelete,
            Exchange exchange
        )
        {
            Name = name;
            RoutingKey = routingKey;
            Exchange = exchange;

            Exchange.Channel.RabbitChannel.QueueDeclare(Name, false, false, false);
            Exchange.Channel.RabbitChannel.QueueBind(Name, Exchange.Name, RoutingKey);
        }

        public void Send(string message)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(message);
            Exchange.Channel.RabbitChannel.BasicPublish(Exchange.Name, RoutingKey, null, bytes);
        }

        public Consumer CreateConsumer(bool autoAck = false)
        {
            return new Consumer(autoAck, this);
        }
    }
}
